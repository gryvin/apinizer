create database if not exists liveu_db 
character set utf8 collate utf8_general_ci;
use liveu_db;
 
create table if not exists `seans` (
 `uuid` varchar(20) not null, 
 `day` varchar(10) not null, 
 `devicename` varchar(50) not null,  
 `formattedAddress` varchar(200),
 `start` datetime not null,  
 `end` datetime not null,  
 primary key(`uuid`)
);
 
create table if not exists `point` ( 
 `id` smallint unsigned not null auto_increment,
 `ts` timestamp,
 `uuid` varchar(20) not null, 
 `csv`  varchar(200) not null, 
 primary key(`id`, `uuid`)
);
 
create table if not exists `user` (
 `id` smallint unsigned not null auto_increment,
 `name` varchar(20) not null,
 `pw` varchar(32) not null,
 primary key(`id`)
);

INSERT INTO user (name, pw) VALUES ('user', '8fe4c11451281c094a6578e6ddbf5eed');

