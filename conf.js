var q1 = 'SELECT m.date, c.name, m.sum FROM money m JOIN consumer c ON c.id = m.consumer_id WHERE `date` >= DATE(?) AND `date` <= DATE(?) ORDER BY m.date', 
q2 = 'SELECT c.id, c.name, SUM(m.sum) as sum FROM money m JOIN consumer c ON c.id = m.consumer_id WHERE `date` >= DATE(?) AND `date` <= DATE(?) GROUP BY c.id ORDER BY c.id';
 
module.exports = {
 'table': {'query': q1, 'data': 'mydata1'}, 
 'bar': {'query': q1, 'data': 'mydata1'}, 
 'pie': {'query': q2, 'data': 'mydata2'}, 
 'column': {'query': q2, 'data': 'mydata2'}
};