const db = require('./db.js');
const server = require('./server.js');
const log = server.log;
const uid2 = require('uid2');
const moment = require('moment');


var device = {}
	,seans = {}
	,businfo = {};
var router = [
  [ "devices/fetch?", /\/luc\/luc\-core\-web\/rest\/devices\/fetch\?/i, Fdevices,
  { //properties to fetch from msg
  "uid" : "Boss100_0620011888a057010000000001009e3a",
  "name" : "501542-51694",
  "type" : "LU-500",
  "swVersion" : "6.0.0.C11629.G5353db9",
  "serialNumber" : "501542-51694"
  }
],
[".erraiBus?",/\.erraiBus\?/i, Fbus, 
  { 
  "message":"", 
  "json":""
  }
],
[ "devices/fetch_coords?", /\/luc\/luc\-core\-web\/rest\/devices\/fetch_coords\?/i, Fcoords,
  {
  "uid" : "Boss100_10e0000c1a3a5a010000000001004232",
  "lat" : "56.329597",
  "lng" : "43.967793",
  "formattedAddress" : "Vladimir, pr.Mira 12"
  }
]
]
chrome.extension.onMessage.addListener(function(msg){
		message(msg.url, msg.resp)
});

function isNormalInteger(str) {
    return /^\+?(0|[1-9]\d*)$/.test(str);
}

function message(url, msg) {
	for(var i=0;i<router.length;i++)
		if( router[i][1].test(url) ) {
		  try{
			var obj = JSON.parse(msg, 
			  function(k, v) {
				if (k === '') { return v; } 
				return (isNormalInteger(k) || typeof router[i][3][k] != "undefined")? v: undefined;     
			  }
			)
		  }catch(e){			  
			log.err(router[i][0]+" message JSON SyntaxError", msg)
		  }
		   router[i][2](obj)	
		}
}

function Fdevices(omsg){	
	device = {};
	var msg;
	for(var i=0;i<omsg.length;i++){
		msg = omsg[i];
//		if(msg["type"] == "LU-500"){
			device[msg["uid"]] = {
				"name" : msg["name"],
				"type" : msg["type"],
				"swVersion" : msg["swVersion"],
				"serialNumber" : msg["serialNumber"]			
//			}
		}
	}
	log.warn("DEVICES", device)
}
function Fcoords(omsg){
	var msg, uid;
	for(var i=0;i<omsg.length;i++){
		msg = omsg[i];
		uid = msg["uid"];
		if(typeof device[uid] == "object"){
			device[uid].lat =  msg["lat"];
			device[uid].lng =  msg["lng"];
			device[uid].formattedAddress =  msg["formattedAddress"];			
		}
	}
	  log.warn("COORDS", device)
}

const busFilter =  { //properties to fetch from "json""
  "object":""
  ,"parts":""
  ,"type":""
  ,"part":""
  ,"deviceId":""
  ,"state":""  
  ,"modems":""
  ,"connected":true
  ,"name":"wifi"
  ,"port":"WLAN-1"
  ,"technology":""
  ,"kbps":637
}
	
function Fbus(omsg){
    for(var i=0;i<omsg.length;i++){
	  try{			  
        var nmsg = JSON.parse(omsg[i].message.json
			  ,function(k, v) {
				if (k === '') { return v; } 
				return (isNormalInteger(k) || typeof busFilter[k] != "undefined")? v: undefined;     
			  }
        );
	    for(var k=0; k<nmsg.object.parts.length; k++){
		  var opart = nmsg.object.parts[k];	
			if(opart.type == "DEVICE_PART"){
			var part = opart.part;
			var uid = part.deviceId;
log(uid+" "+device[uid].name+" "+part.state, seans[uid])			
			if(typeof device[uid] != "undefined") {  //device exists in dictionary -> type == "LU-500"		
				if(part.state == "streaming" || part.state == "storeandforward"){
					if(typeof seans[uid] == "undefined") 
						seansOn(uid, part);
					
					var kbps = [];
					for(var i=0;i< part.modems.length; i++){
						var pm = part.modems[i];
						if(pm.connected) 
						  kbps.push(pm.kbps);
					}
					db.savePoint(seans[uid].uuid, kbps);
				}else
					seansOff(uid);
			}
		}
		}
	  }catch(e) {log.err("BUS message inner json SyntaxError", omsg) }
	}
 
}

function seansOff(uid){
		if(seans[uid]){
			seans[uid].end = new Date();
			db.updateSeans(seans[uid]);
			delete seans[uid];			
			log("seans deleted",uid)
		}
}
function seansOn(uid, part){
	log("seansON   ",uid)
	log("old seans",seans)	

		seans[uid] = {
			uuid : moment().format("MM-DD-YYYY")+uid2(5)
		    ,day : moment().format("MM-DD-YYYY")
			,devicename : device[uid].name
			,formattedAddress : device[uid].formattedAddress
			,start: new Date()
			,end: new Date()
		};
		log("new seans",seans)
		db.insertSeans(seans[uid]);
		
		var labels = [];
		for(var i=0;i< part.modems.length; i++){
			var pm = part.modems[i];
			if(pm.connected) 
				labels.push(pm.name+" "+pm.technology+":"+pm.port);
		}
		log("labels", labels)	
		db.savePoint(seans[uid].uuid, labels);		
}


