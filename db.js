var pjson = require('./props.js')
var crypto = require('crypto');
var mysql = require('mysql');
const server = require('./server.js');
const log = server.log;

var pool = mysql.createPool({
	host      : pjson.db.host
	,user     : pjson.db.user
	,password : pjson.db.password
	,database : pjson.db.database 
	,insecureAuth: true
});

module.exports.auth = function(user, pw, cb) { 
 pool.getConnection(function(err, connection) {
  if (err) {throw err; return;}   
  connection.query('SELECT * FROM user WHERE name = ? AND pw = ?', [user, crypto.createHash('md5').update(pw).digest('hex')], function(err, data) {   
   if (err) {throw err; return;}   
   connection.release();   
   cb(null /* error */, data.length);   
  });   
 }); 
}

module.exports.get = function getData(req, q, cb) {
 pool.getConnection(function(err, connection) {
  if (err) {throw err; return;}  
  var from = req.query.from ? new Date(req.query.from) : new Date();
  var to = req.query.to ? new Date(req.query.to) : new Date();  
  connection.query(q, [from, to], function(err, data) {   
   if (err) {throw err; return;}   
   connection.release();   
   cb(data);
  });   
 }); 
}

module.exports.savePoint = function savePoint(uuid, arr){
	log("savePoint "+uuid, arr)
 pool.getConnection(function(err, connection) {
  if (err) {log("ERR savePoint1  "+uuid, err); return;}   
  connection.query('INSERT INTO `point` SET ?', {"uuid":uuid, "csv": arr.join(",")}
   ,function(err, data) {   
	if (err) {log("ERR savePoint2  "+uuid, err); return;}   
	log("savePoint SUCCESS", data)	
	connection.release();   
   }
  );   
 }); 	
};
module.exports.insertSeans  = function insertSeans(seans){
	log("insertSeans "+seans.uuid,seans)	
 pool.getConnection(function(err, connection) {
  if (err) {log("ERR insertSeans1  "+seans.uuid, err); return;}   
  connection.query('INSERT INTO `seans` SET ?', {"uuid":seans.uuid, "day":seans.day, "devicename":seans.devicename, "formattedAddress":seans.formattedAddress, "start":seans.start, "end":seans.end}
   ,function(err, data) {   
	if (err) {log("ERR insertSeans2  "+seans.uuid, err); return;}   
	log("insertSeans SUCCESS", data)
	connection.release();   
   }
  );   
 }); 	
};

module.exports.updateSeans  = function updateSeans(seans){
	log("updateSeans ", seans)	
 pool.getConnection(function(err, connection) {
  if (err) {log("updateSeans", err); return;}   
  connection.query('UPDATE seans SET ? WHERE ?', [{ "end": seans.end }, { "uuid": seans.uuid }]
   ,function(err, data) {   
	if (err) {log("updateSeans", err); return;}   
	log("updateSeans SUCCESS", data)	
	connection.release();   
   }
  );   
 }); 		
};
