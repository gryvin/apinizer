const express = require('express');
const SSE = require('express-sse');
const routes = require('./routes');
const http = require('http');
const path = require('path');
const app = express();
const db = require('./db'); 
const sse = new SSE([0, 1, 2, 3], { serializeInitial: false });
 
// all environments
app.set('port', process.env.PORT || 3000);
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.bodyParser());
app.use(express.methodOverride());
 
var auth = express.basicAuth(function(user, pw, cb) {
 if (!user || !pw) {cb(null, false); return;}
 db.auth(user, pw, cb); 
});
 
app.use(app.router);
app.use(express.static(path.join(__dirname, 'static')));
 
// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}
 
app.get('/', auth, routes.index);
app.get('/:page((table|bar|pie|column))', auth, routes.chart);
 

 app.get('/log', (req, res) => {
    res.end(`
        <html>
          <body>
<div ondblclick="elist.innerHTML = '';" style="margin: 0 0 10 0;padding:0;height:32%;width:100%;overflow:auto;border:1px solid red;">
            <ul id="errlist">
            </ul>
</div>			
<div ondblclick="wlist.innerHTML = '';" style="margin: 0 0 10 0;padding:0;height:32%;width:100%;overflow:auto;border:1px solid blue;">
            <ul id="warnlist">
            </ul>
</div>			
<div ondblclick="ilist.innerHTML = '';" style="height:32%;width:100%;padding:0;overflow:auto;border:1px solid green;">
            <ul id="infolist">
            </ul>
</div>			

            <script>
              var es = new EventSource('/sse');
              var elist = document.querySelector('#errlist');
			  var wlist = document.querySelector('#warnlist');
			  var ilist = document.querySelector('#infolist');
 
              es.addEventListener('err', function (event) {
                var item = document.createElement('li');
                item.innerText = event.lastEventId+"\\n"+event.data;
                elist.appendChild(item);
              });
              es.addEventListener('warn', function (event) {
                var item = document.createElement('li');
                item.innerText = event.lastEventId+"\\n"+event.data;
                wlist.appendChild(item);
              });
              es.addEventListener('info', function (event) {
                var item = document.createElement('li');
                item.innerText = event.lastEventId+"\\n"+event.data;
                ilist.appendChild(item);
              });
            </script>
          </body>
        </html>
      `);
  });
 
app.get('/sse', sse.init);
 
var logger = function(txt,o){
	sse.send(o, 'info', txt);	
} 
logger.log = function(txt,o){
	sse.send(o, 'info', txt);	
} 
logger.err = function(txt,o){
	sse.send(o, 'err', txt);	
} 
logger.warn = function(txt,o){
	sse.send(o, 'warn', txt);	
} 

module.exports.log = logger;
 
 http.createServer(app).listen(app.get('port'), function(){  
 console.log("Express server listening on port %d in %s mode", app.get('port'), app.settings.env);
});


